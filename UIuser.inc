<?php
/**
 * @file
 *   Retrieve data user UI menggunakan webservices.
 *
 * @version 0.0.5
 *
 * @author IjorTengab
 *
 * @homepage https://kelola.ui.ac.id/issues/65866
 *
 * @todo
 *  - Buat fitur cache dan simpan sebagai file pada direktori /tmp.
 */

/**
 * Class UIuser.
 *
 * List of Content:
 *  - Description
 *  - Requirement
 *  - Fitur
 *  - Panduan Extends
 *
 * Description:
 * Class UIuser dibuat untuk memudahkan pencarian data user melalui web service,
 * dan LDAP Server. Jika pada suatu kasus server web service down atau
 * hasil NULL pada web service, maka pencarian akan dicoba ke LDAP Server.
 *
 * Data yang dapat dicari meliputi nama lengkap, fakultas, program studi, status
 * akademik, dan sebagainya (lihat pada sub bab "Fitur"). Jika ingin
 * meng-extends class ini, harap perhatikan sub bab "Panduan Extends".
 *
 * Requirement:
 *  - Pastikan IP server anda diijinkan untuk mengakses Services UI, terutama
 *    pada fungsi:
 *     - DataDiriByUserID
 *     - DataDiriByNama
 *     - DataDiriByNIP
 *     - DataAccountMahasiswaUiByUserName
 *     - DataAccountMahasiswaUiByNama
 *     - DataProdiEpsbedFakultas
 *     - DaftarProdiSiakEpsbed
 *     - DataProdiEpsbed
 *
 *  - (optional) Curl library. Secara default, maka class ini akan menggunakan
 *    curl sebagai media browsing. Namun jika tidak ada, maka otomatis akan
 *    menggunakan fungsi file_get_contents.
 *
 *  - Konfigurasi allow_url_fopen = TRUE, pada php.ini (hanya jika menggunakan
 *    fungsi file_get_content sebagai media browsing).
 *
 * Fitur:
 *  1. Memudahkan untuk mengambil informasi user seperti nama, fakultas,
 *     organisasi, alamat, foto, dan sebagainya. Sumber pertama pencarian data
 *     yakni webservice, jika tidak ada, maka sumber pencarian berikutnya yakni
 *     LDAP.
 *
 *     @code
 *       $nama = UIuser::getInfo('muhammad.roji')->nama;
 *     @endcode
 *     Eksekusi code diatas akan mendapatkan nama lengkap dari username
 *     muhammad.roji.
 *
 *     @code
 *       $foto = UIuser::getInfo('muhammad.roji')->foto_base64;
 *     @endcode
 *     Eksekusi code diatas akan mendapatkan foto dalam format base64. Untuk
 *     implementasi sebagai element html, gunakan kode berikut:
 *     @code
 *       $html = '<img src="data:image/jpg;base64,' . $foto . '" alt="" />';
 *     @endcode
 *     . Untuk menyimpan sebagai file, maka gunakan kode berikut:
 *     @code
 *       $binary = base64_decode($foto);
 *       $file_name = 'foto.jpg';
 *       file_put_contents($file_name, $binary);
 *     @endcode
 *
 *     @code
 *       $info = UIuser::getInfo('muhammad.roji');
 *     @endcode
 *     Eksekusi code diatas akan mendapatkan semua informasi dari username
 *     muhammad.roji (output berupa standard class).
 *
 *     Class ini menggunakan static sebagai cache temporary. Misalnya:
 *     @code
 *       $info_1 = UIuser::getInfo('muhammad.roji');
 *       $info_2 = UIuser::getInfo('anme51');
 *       $info_3 = UIuser::getInfo('muhammad.roji');
 *     @endcode
 *     Pada variable $info_1, class mencari data via web service/LDAP, sementara
 *     pada variable $info_3 data diambil dari static variable,
 *     sehingga tidak perlu request kembali ke web service atau LDAP.
 *
 *  2. Memudahkan untuk mengambil data fungsi apapun dari web service.
 *     Contoh:
 *     @code
 *       $json = UIuser::webServicesBrowse('DataDiriByNama', 'Muhammad Roji');
 *     @endcode
 *     Eksekusi code diatas akan mendapat hasil yang sama dengan
 *     mengunjungi URL:
 *     @link https://services.ui.ac.id/ws/middleware/?f=DataDiriByNama&1=Muhammad+Roji&args=1&wkt=now @endlink
 *
 *     @code
 *       $json = UIuser::webServicesBrowse('MahasiswaAktif', '2010', '1');
 *     @endcode
 *     Eksekusi code diatas akan mendapat hasil yang sama dengan
 *     mengunjungi URL:
 *     @link https://services.ui.ac.id/ws/middleware/?f=MahasiswaAktif&args=2&1=2010&2=1&wkt=now @endlink
 *
 *     @code
 *       $json = UIuser::webServicesBrowse3('sipeg_personal', 'muhammad.roji');
 *     @endcode
 *     Eksekusi code diatas akan mendapat hasil yang sama dengan
 *     mengunjungi URL:
 *     @link https://services.ui.ac.id/ws/middleware/index3.php?f=sipeg_personal&1=muhammad.roji&args=1&wkt=now @endlink
 *
 *  3. Memudahkan untuk mengambil data user dari LDAP.
 *     Contoh:
 *     @code
 *       $array = UIuser::infoLDAP('muhammad.roji');
 *     @endcode
 *     Eksekusi code diatas akan mendapat informasi user dari LDAP yang telah
 *     dimapping. Untuk hasil mentah tanpa mapping, maka gunakan code berikut:
 *     @code
 *       $array = UIuser::infoLDAP('muhammad.roji', TRUE);
 *     @endcode
 *
 *
 * Panduan Extends:
 *   @todo
 *
 */
class UIuser {

  /**
   * Tempat penyimpanan data untuk peningkatan performa,
   * terutama terkait data yang digunakan berulang-ulang pada satu eksekusi.
   */
  public static $storage = [];

  /**
   * Setting timeout saat browsing. Tidak berlaku jika value pada property
   * $method_preferred_client_browser bernilai "_file_get_contents".
   */
  public static $timeout = 30;

  /**
   * Alamat URL untuk mengakses web services.
   */
  protected static $url_services = 'https://services.ui.ac.id/ws/middleware/index.php';

  /**
   * Alamat URL alternative untuk mengakses informasi staf, khususnya foto.
   */
  protected static $url_services_3 = 'https://services.ui.ac.id/ws/middleware/index3.php';

  /**
   * Method yang digunakan untuk melakukan browsing.
   * Alternatif selain value "_curl" adalah "_file_get_contents".
   * Keunggulan menggunakan curl yakni ada opsi untuk timeout.
   *
   * Jika meng-extends class ini, maka anda dapat custom untuk method lain, dan
   * menggunakan library lain, contoh: class http guzzle, atau
   * fungsi drupal_http_request().
   *
   * Method harus mereturn string dari HTTP_message_body (tanpa HTTP header).
   */
  protected static $method_preferred_client_browser = '_curl';

  /**
   * Data hasil tembak ke middleware atau LDAP ditampung disini.
   * Ada atau tidak ada data, maka property $result ini akan berisi array yang
   * bersumber dari method info_user() sebagai fungsi referensi array.
   */
  public $result;

  /**
   * Informasi username, didefinisikan saat construct.
   */
  protected $username;

  /**
   * Alamat Host LDAP server.
   */
  protected static $ldap_host = '152.118.39.37';

  /**
   * Base DN dari LDAP server.
   */
  protected static $ldap_basedn = 'o=Universitas Indonesia, c=ID';

  /**
   * Construct.
   */
  function __construct($username = NULL) {
    // Name HARUS dibuat lowercase, contoh
    // mengambil hasil dari fungsi DataDiriByUserID
    // dengan argument Widyawati menghasilkan null,
    // mengambil hasil dari fungsi DataDiriByUserID
    // dengan argument widyawati menghasilkan value,
    // begitu pula dengan account lilis2010.
    $this->username = strtolower($username);
  }

  /**
   * Fungsi static untuk mengakses informasi user.
   * User disini bisa sebagai staf atau mahasiswa, dimana asumsi pertama ialah
   * sebagai staf, jika hasil tidak ketemu, maka asumsi kedua adalah mahasiswa.
   *
   * @param $username
   *   Username atau dikenal juga sebagai UserID
   *
   * Return
   *   Standar Object yang berisi informasi user. Isi dari property result ini
   *   dapat dilihat pada method info_user().
   */
  public static function getInfo($username) {

    // Periksa object yang dilampirkan pada variable static $storage,
    // jika tidak ada, maka buat instance baru.
    $storage = UIuser::$storage;
    if (!isset($storage[$username])) {
      UIuser::$storage[$username] = new UIuser($username);
    }
    $current_object = UIuser::$storage[$username];

    // Jika property result masih NULL, artinya belum ada tembakan ke
    // webservices atau LDAP Server.
    if (is_null($current_object->result)) {
      // Tembak ambil data.
      $current_object->getInfoUser();
    }
    return $current_object->result;
  }

  /**
   * Method untuk mengunjungi services UI.
   *
   * @param $function
   *   Nama fungsi untuk memanggil services.
   *
   * Argument lain yang dipassing setelah parameter $function akan digunakan
   * sebagai argument untuk memanggil services.
   *
   * Return
   *   Mengembalikan mentah-mentah hasil dari web services.
   */
  public static function webServicesBrowse($function) {
    // Construct URL.
    $args = func_get_args();
    array_shift($args);
    $output = '';
    if (!empty($args)) {
      $query = [];
      $query['f'] = $function;
      $query['args'] = count($args);
      $x = 0;
      do {
        $query[++$x] = array_shift($args);
      } while(!empty($args));
      $query['wkt'] = 'now';
    }
    $url = self::$url_services . '?' . http_build_query($query);
    $method_preferred = self::$method_preferred_client_browser;
    return self::{$method_preferred}($url);
  }

  /**
   * Mengakses web services versi dengan alaman URL yang berbeda dari default.
   * @see self::webServicesBrowse().
   */
  public static function webServicesBrowse3($function) {
    self::$url_services = self::$url_services_3;
    return call_user_func_array(array('self', 'webServicesBrowse'), func_get_args());
  }

  /**
   * Fungsi shortcut untuk browsing data di LDAP.
   *
   */
  public static function LDAPbrowse($host, $base_dn, $filter) {
    if ($ldapconn = ldap_connect($host)) {
      $sr = ldap_search($ldapconn, $base_dn, $filter);
      return ldap_get_entries($ldapconn, $sr);
    }
  }

  /**
   * Mendapat informasi user via LDAP.
   *
   */
  public static function infoLDAP($username, $raw = FALSE) {
    $object = new UIuser($username);
    return $object->getInfoUserFromLDAPserver($raw);
  }

  /**
   * Cara paling gampang untuk mengambil data via URL tanpa mempedulikan
   * header HTTP yakni menggunakan fungsi file_get_contents();
   */
  protected static function _file_get_contents($url) {
    return file_get_contents($url);
  }

  /**
   * Cara lain mendapatkan data via URL yakni dengan curl.
   */
  protected static function _curl($url) {
    if (!function_exists('curl_init')) {
      return self::_file_get_contents($url);
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_TIMEOUT, self::$timeout);
    curl_setopt($ch, CURLOPT_URL, $url);
    $response = curl_exec($ch);
    return $response;
  }

  /**
   * Method utama untuk mendapatkan informasi user, dari sini akan dicoba
   * terlebih dahulu ke webservices, jika tidak ada hasil maka akan dicoba ke
   * LDAP server.
   * Hasil dari fungsi ini ialah diisinya property $result dari NULL menjadi
   * standar object.
   */
  protected function getInfoUser() {
    $result = $this->getInfoUserFromWebServices();
    // Jika value $result FALSE, maka berarti web service down.
    // Jika value $result empty array, maka berarti web service tidak ada data,
    // bisa jadi data belum di mirror atau data benar-benar tidak ada.
    // Coba cari via LDAP.
    if (empty($result)) {
      $result = $this->getInfoUserFromLDAPserver();
    }
    // Akhir dari fungsi ini perlu diset agar property $result tidak lagi NULL.
    $final = $this->info_user();
    if (!empty($result)) {
      $final = $result;
    }
    $this->result = (object) $final;
  }

  /**
   * Mengambil data user dari web service.
   */
  protected function getInfoUserFromWebServices() {
    $args = func_get_args();
    if (empty($args)) {
      // Test sebagai staff.
      $result = $this->getInfoUserFromWebServices(array('self', 'webServicesBrowse'), array('DataDiriByUserID', $this->username));
      if (empty($result)) {
        // Coba test sebagai mahasiswa.
        $result = $this->getInfoUserFromWebServices(array('self', 'webServicesBrowse'), array('DataAccountMahasiswaUiByUserName', $this->username));
      }
      // Jika didapat user sebagai staf, maka tembak lagi dengan index3.
      if (isset($result['is_staf']) && $result['is_staf']) {
        $result_added = $this->getInfoUserFromWebServices(array('self', 'webServicesBrowse3'), array('sipeg_personal', $this->username));
        // Kita akan merge dengan data yang sudah exist pada service sebelumnya,
        // maka kita filter terlebih dahulu, agar tidak ketimpa data yang empty.
        $result_added = array_filter($result_added);
        $result = array_merge($result, $result_added);
      }
      return $result;
    }
    $callback = $args[0];
    $param_arr = $args[1];
    $function = $args[1][0];
    $http_body = call_user_func_array($callback, $param_arr);
    $result = json_decode($http_body, TRUE);
    if (is_array($result)) {
      $result = $this->simplifyResultFromWebServices($result);
      if (!empty($result)) {
        switch ($function) {
          case 'sipeg_personal':
            $result = $this->mappingResultFromWebServices3_staf($result);
            break;

          case 'DataDiriByUserID':
            $result = $this->mappingResultFromWebServices_staf($result);
            break;

          case 'DataAccountMahasiswaUiByUserName':
            $result = $this->mappingResultFromWebServices_mahasiswa($result);
            break;
        }
      }
    }
    return $result;
  }

  /**
   * Mengambil data user dari LDAP.
   *
   */
  protected function getInfoUserFromLDAPserver($raw = FALSE) {
    $filter = 'uid=' . $this->username;
    $result = self::LDAPbrowse(self::$ldap_host, self::$ldap_basedn, $filter);
    if ($raw) {
      return $result;
    }
    $result = $this->simplifyResultFromLDAPServer($result);
    if (!empty($result)) {
      $result = $this->mappingResultFromLDAPserver($result);
    }
    return $result;

    // if ($ldapconn = ldap_connect($this->ldap_host)) {
      // $sr = ldap_search($ldapconn, $this->ldap_basedn, $filter);
      // $result = ldap_get_entries($ldapconn, $sr);
      // $result = $this->simplifyResultFromLDAPServer($result);
      // if (!empty($result)) {
        // $result = $this->mappingResultFromLDAPserver($result);
      // }
      // return $result;
    // }
  }

  /**
   * Menyederhanakan output hasil request data ke web service.
   */
  protected function simplifyResultFromWebServices($result) {
    $output = [];
    if(!empty($result['data'])){
      foreach($result['data'] as $data){
        $item = [];
        $kolom = $result['kolom'];
        // Pada web service index3.php nilai $kolom = NULL
        if (!isset($kolom) && isset($result['data'][0])) {
          $kolom = array_keys($result['data'][0]);
        }
        // Pastikan kolom isset.
        if (!empty($kolom)) {
          foreach($kolom as $field) {
            $item[$field] = $data[$field];
          }
          $output[] = $item;
        }
      }
    }
    return $output;
  }

  /**
   * Menyederhanakan output hasil request data ke LDAP.
   */
  protected function simplifyResultFromLDAPServer($result) {
    $output = [];
    if (!empty($result['count'])) {
      $index = 0;
      $count = $result['count'];
      do {
        $item = [];
        $each = $result[$index];
        foreach ($each as $key => $info) {
          if (is_numeric($key)) {
            $data = $each[$each[$key]];
            $count = $data['count'];
            unset($data['count']);
            if ($count == 1) {
              $data = array_shift($data);
            }
            $item[$each[$key]] = $data;
          }
        }
        $count--;
        $index++;
        $output[] = $item;
      } while($count);
    }
    return $output;
  }

  /**
   * Memasukkan hasil dari method simplifyResultFromWebServices()
   * ke array dari method info_user().
   */
  protected function mappingResultFromWebServices_staf($result) {
    $delta = 0;
    $is_dosen = FALSE;
    $fakultas_multi = $prodi_multi = $organisasi_multi = [];
    $fakultas = $prodi = $organisasi = NULL;
    $output = $this->info_user();

    // Cari delta (key pada array) dengan berpatokan pada kolom 'unit utama'
    // Cari informasi apakah dia dosen atau bukan, berpatokan pada kolom 'siak'
    foreach($result as $key => $value) {
      if($value['unit_utama'] == 't') {
        $delta = $key;
      }
      if($value['siak'] == 't') {
        $is_dosen = TRUE;
      }
      empty($value['fakultas']) or $fakultas_multi[] = $value['fakultas'];
      empty($value['organisasi']) or $organisasi_multi[] = $value['organisasi'];
      if (stripos($value['fakultas'], 'fakultas') === 0 or stripos($value['fakultas'], 'program') === 0) {
        $fakultas = $value['fakultas'];
      }
    }

    $output['status'] = $result[$delta]['status'];
    $output['nip'] = $result[$delta]['nip'];
    $output['nidn'] = $result[$delta]['nidon'];
    $output['nama'] = $result[$delta]['nama'];
    $output['gelar_depan'] = $result[$delta]['gelar_depan'];
    $output['gelar_belakang'] = $result[$delta]['gelar_belakang'];
    $output['alamat'] = $result[$delta]['alamat'];
    $output['kelamin'] = $result[$delta]['kelamin'];
    $output['lahir_tempat'] = $result[$delta]['lahir_tmp'];
    $output['agama'] = $result[$delta]['agama'];
    $output['jenjang_pendidikan'] = $result[$delta]['pendidikan'];
    $output['fakultas'] = $fakultas;
    $output['fakultas_multi'] = array_unique($fakultas_multi);
    $output['organisasi'] = $result[$delta]['organisasi'];
    $output['organisasi_multi'] = array_unique($organisasi_multi);
    $output['is_ui'] = TRUE;
    $output['is_staf'] = TRUE;
    $output['is_dosen'] = $is_dosen;

    // Nama + gelar.
    if (!empty($output['nama'])) {
      $nama_gelar = $output['nama'];
      empty($output['gelar_depan']) or $nama_gelar = $output['gelar_depan'] . ' ' . $nama_gelar;
      empty($output['gelar_belakang']) or $nama_gelar = $nama_gelar . ', ' . $output['gelar_belakang'];
      $output['nama_gelar'] = $nama_gelar;
    }

    // Email.
    if (!empty($result[$delta]['email'])) {
      // Contoh nilai yang muncul ternyata ada yang seperti ini:
      // dahlia-s@ui.ac.id, dahlia.sarie@ui.ac.id, jadi kita perlu siasati.
      $split = preg_split('/(,|\s)/', $result[$delta]['email'] );// pisah dengan koma atau space
      if(!empty($split)) {
        $email_ui = $email_non_ui = [];
        foreach($split as $mail) {
          $validate = (bool) filter_var($mail, FILTER_VALIDATE_EMAIL);
          if ($validate) {
            $var = preg_match('/ui.ac.id$/', $mail) ? 'email_ui' : 'email_non_ui';
            ${$var}[] = $mail;
          }
        }
        $output['email_ui_multi'] = $email_ui;
        $output['email_non_ui_multi'] = $email_non_ui;
        $output['email_multi'] = array_merge($email_ui, $email_non_ui);
      }
      // Populate field single value with first index.
      empty($output['email_ui_multi'][0]) or $output['email_ui'] = $output['email_ui_multi'][0];
      empty($output['email_non_ui_multi'][0]) or $output['email_non_ui'] = $output['email_non_ui_multi'][0];
      empty($output['email_multi'][0]) or $output['email'] = $output['email_multi'][0];
    }
    return $output;
  }

  /**
   * Memasukkan hasil dari method simplifyResultFromWebServices()
   * ke array dari method info_user().
   */
  protected function mappingResultFromWebServices3_staf($result) {
    $output = $this->info_user();
    $delta = 0;
    $output['foto_base64'] = $result[$delta]['foto'];
    $output['golongan'] = $result[$delta]['golongan'];
    $output['jabatan'] = $result[$delta]['jabatan'];
    return $output;
  }

  /**
   * Memasukkan hasil dari method simplifyResultFromWebServices()
   * ke array dari method info_user().
   */
  protected function mappingResultFromWebServices_mahasiswa($result) {
    $fakultas_multi = $prodi_multi = $organisasi_multi = [];
    $fakultas = $prodi = $organisasi = NULL;
    $output = $this->info_user();
    $delta = 0;
    foreach($result as $key => $value) {
      $fakultas = $this->translateFaculty($value['kd_org'], 'prodi');
      empty($fakultas) or $fakultas_multi[] = $fakultas;
      empty($value['kd_org']) or $prodi_multi[] = $value['kd_org'];
    }
    $output['status'] = $result[$delta]['status'];
    $output['nama'] = $result[$delta]['nama'];
    $output['nama_gelar'] = $result[$delta]['nama'];
    $output['npm'] = $result[$delta]['npm'];
    $output['alamat'] = $result[$delta]['alamat'];
    $email_ui = $result[$delta]['email_ui'];
    $email_luar = $result[$delta]['email_luar'];
    $output['email'] = $email_ui;
    $output['email_ui'] = $output['email_ui_multi'][] = $email_ui;
    $output['email_non_ui'] = $output['email_non_ui_multi'][] = $email_luar;
    $output['email_multi'] = array_merge($output['email_ui_multi'], $output['email_non_ui_multi']);
    $output['fakultas_multi'] = array_unique($fakultas_multi);
    $output['prodi_multi'] = array_unique($prodi_multi);
    $output['is_ui'] = TRUE;
    $output['is_mahasiswa'] = TRUE;
    // Populate field single value with first index.
    empty($output['fakultas_multi'][0]) or $output['fakultas'] = $output['fakultas_multi'][0];
    empty($output['prodi_multi'][0]) or $output['prodi'] = $output['prodi_multi'][0];
    return $output;
  }

  /**
   * Memasukkan hasil dari method simplifyResultFromLDAPServer()
   * ke array dari method info_user().
   */
  protected function mappingResultFromLDAPserver($result) {
    // Gunakan hanya pada delta 0.
    $delta = 0;
    $result = $result[$delta];
    $output = $this->info_user();
    $output['nama'] = $result['cn'];
    $output['email'] = $result['mail'];
    $email_ui = $email_non_ui = [];
    $var = preg_match('/ui.ac.id$/', $result['mail']) ? 'email_ui' : 'email_non_ui';
    ${$var}[] = $result['mail'];
    $output['email_ui_multi'] = $email_ui;
    $output['email_non_ui_multi'] = $email_non_ui;
    $output['email_multi'] = array_merge($email_ui, $email_non_ui);
    empty($output['email_ui_multi'][0]) or $output['email_ui'] = $output['email_ui_multi'][0];
    empty($output['email_non_ui_multi'][0]) or $output['email_non_ui'] = $output['email_non_ui_multi'][0];
    // untuk mengecek staff bisa dengan melihat pada
    // key role == staf|sba|operator-ppsi, TETAPI
    // pada kasus username drpm, editor_makara, editor_mst, editor_msh, warek1
    // juga memiliki role == staf|sba|operator-ppsi padahal mereka
    // bukan account individu,
    // jadi kita gunakan key idstaf sebagai penentu.
    // TETAPI account muhammad.roji dan alim ternyata
    // key idstaf == empty string,
    // padahal kami adalah staf, jadi sebaiknya menggunakan
    // key hasaccessto sipeg.ui.ac.id
    if (is_array($result['hasaccessto']) && in_array('sipeg.ui.ac.id', $result['hasaccessto'])) {
      $output['is_staf'] = TRUE;
    }
    // untuk mencari mahasiswa cukup sederhana,
    // cukup dengan melihat pada key role
    if ($result['role'] == 'mahasiswa') {
      $output['is_mahasiswa'] = TRUE;
    }
    // mencari tahu dosen
    // jika dosen, maka juga pasti staf.
    if ($result['role'] == 'dosen') {
      $output['is_staf'] = TRUE;
      $output['is_dosen'] = TRUE;
    }
    if (isset($result['kodeorg'])) {
      $kodeorgs = (array) $result['kodeorg'];
      $fakultas_multi = $prodi_multi = [];
      foreach ($kodeorgs as $kodeorg) {
        $pola = '/(?P<prodi>[^\.]+\.[^\.]+\.[^\.]+\.[^\.]+)\:(?P<role>.*)/';
        preg_match($pola, $kodeorg, $match);
        if (!empty($match)) {
          if ($match['role'] == 'dosen') {
            $output['is_staf'] = TRUE;
            $output['is_dosen'] = TRUE;
          }
          elseif ($match['role'] == 'mahasiswa') {
            $output['is_mahasiswa'] = TRUE;
          }
          $fakultas = $this->translateFaculty($match['prodi'], 'prodi');
          empty($fakultas) or $fakultas_multi[] = $fakultas;
          if (strpos($match['prodi'], 'x') === FALSE) {
            $prodi_multi[] = $match['prodi'];
          }
        }
      }
      $output['fakultas_multi'] = array_unique($fakultas_multi);
      $output['prodi_multi'] = array_unique($prodi_multi);
      empty($fakultas_multi[0]) or $output['fakultas'] = $fakultas_multi[0];
      empty($prodi_multi[0]) or $output['prodi'] = $prodi_multi[0];
    }
    $output['is_ui'] = TRUE;
    // Kode identitas.
    if ($output['is_staf']) {
      $output['nip'] = $result['kodeidentitas'];
    }
    elseif ($output['is_mahasiswa']) {
      $output['npm'] = $result['kodeidentitas'];
    }
    // Tambahan informasi
    !isset($result['homepostaladdress']) or $output['alamat'] = $result['homepostaladdress'];
    return $output;
  }

  /**
   * Mengubah kode fakultas/kode prodi menjadi informasi fakultas.
   */
  protected function translateFaculty($kode, $type = 'fakultas') {
    if ($type == 'prodi') {
      $kode = substr($kode, -5, 2);
    }
    $fakultas_name = '';
    switch ($kode) {
      case '01':
        $fakultas_name = 'Fakultas Kedokteran';
        break;

      case '02':
        $fakultas_name = 'Fakultas Kedokteran Gigi';
        break;

      case '03':
        $fakultas_name = 'Fakultas Matematika dan Ilmu Pengetahuan Alam';
        break;

      case '04':
        $fakultas_name = 'Fakultas Teknik';
        break;

      case '05':
        $fakultas_name = 'Fakultas Hukum';
        break;

      case '06':
        $fakultas_name = 'Fakultas Ekonomi';
        break;

      case '07':
        $fakultas_name = 'Fakultas Ilmu Pengetahuan Budaya';
        break;

      case '08':
        $fakultas_name = 'Fakultas Psikologi';
        break;

      case '09':
        $fakultas_name = 'Fakultas Ilmu Sosial dan Ilmu Politik';
        break;

      case '10':
        $fakultas_name = 'Fakultas Kesehatan Masyarakat';
        break;

      case '12':
        $fakultas_name = 'Fakultas Ilmu Komputer';
        break;

      case '13':
        $fakultas_name = 'Fakultas Ilmu Keperawatan';
        break;

      case '14':
        $fakultas_name = 'Program Pasca Sarjana';
        break;

      case '15':
        $fakultas_name = 'Program Vokasi';
        break;

      case '17':
        $fakultas_name = 'Fakultas Farmasi';
        break;
    }
    return $fakultas_name;
  }

  /**
   * Array sebagai referensi dari info user.
   */
  protected function info_user() {
    return array(
      'status' => NULL,
      'nama' => NULL,
      'nama_gelar' => NULL,
      'nip' => NULL,
      'npm' => NULL,
      'nidn' => NULL,
      'gelar_depan' => NULL,
      'gelar_belakang' => NULL,
      'alamat' => NULL,
      'email' => NULL,
      'email_multi' => [],
      'email_ui' => NULL,
      'email_ui_multi' => [],
      'email_non_ui' => NULL,
      'email_non_ui_multi' => [],
      'kelamin' => NULL,
      'lahir_tempat' => NULL,
      'lahir_tanggal' => NULL,
      'agama' => NULL,
      // Contoh jenjang pendidikan ialah : S1, S0, S2, S3,
      'jenjang_pendidikan' => NULL,
      'program_pendidikan' => NULL,
      'fakultas' => NULL,
      'fakultas_multi' => [],
      'prodi' => NULL,
      'prodi_multi' => [],
      'organisasi' => NULL,
      'organisasi_multi' => [],
      'is_ui' => FALSE,
      'is_dosen' => FALSE,
      'is_staf' => FALSE,
      'is_mahasiswa' => FALSE,
      'foto_url' => NULL,
      'foto_base64' => NULL,
      'golongan' => NULL,
      'jabatan' => NULL,
    );
  }

}