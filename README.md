UI User
=======

Description
-----------
Module untuk menghandle semua kebutuhan mengenai user di UI. Module ini 
merupakan successor dari module UI Middleware Integration.

Requirements
------------
 - IP Server harus diijinkan untuk mengakses Services UI.
 - PHP 5.6

Feature
-------
 - Membuat entity baru untuk menampung informasi dari Services.
 - Membuat cache untuk optimasi sinkronisasi.
 - Auto configuration CAS dan atau LDAP.
 - Autoload informasi Fakultas dan Program Studi.
 - 


, seperti
sinkronisasi dengan services ui, autocreate user, auto konfigurasi CAS dan atau
LDAP, dan lain-lain.